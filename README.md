## Installasjon

1. `cd %prosjekter-mappe%`
2. `git clone git@gitlab.com:zpirit/opp-as.git`
3. `cd opp-as`

## Oppgave

Opprett nødvendige html-element og gi dem stilen som trengs via CSS.  
Hvordan det skal se ut og filer som trengs ligger i mappen 'Prosjektfiler'.
